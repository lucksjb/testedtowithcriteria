package com.example.teste.dtoout;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Setter
@Getter
@ToString
public class TabcDTOout {
    private Long id;
    private String descricao;
    private List<TabdDTOout> items = new ArrayList<>();

    public TabcDTOout(Long id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }    

}
