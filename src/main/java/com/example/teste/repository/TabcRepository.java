package com.example.teste.repository;

import com.example.teste.models.Tabc;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TabcRepository extends JpaRepository<Tabc,Long>, TabcRepositoryQuery {
    
}
