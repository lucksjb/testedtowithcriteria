package com.example.teste.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.example.teste.dtoout.TabcDTOout;
import com.example.teste.models.Tabc;

public class TabcRepositoryImpl implements TabcRepositoryQuery{

    @PersistenceContext
    private EntityManager manager;

    @Override
    public List<TabcDTOout> findx() {
        CriteriaBuilder cb = manager.getCriteriaBuilder();
        CriteriaQuery<TabcDTOout> cq  = cb.createQuery(TabcDTOout.class);
        Root<Tabc> rootFrom = cq.from(Tabc.class);

        cq
                .select(cb.construct(TabcDTOout.class, 
                rootFrom.get("descricao"), 
                rootFrom.get("items")
                ));
        TypedQuery<TabcDTOout> query = manager.createQuery(cq);
        return query.getResultList();
    }
    
}
