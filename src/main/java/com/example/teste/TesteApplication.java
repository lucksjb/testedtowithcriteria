package com.example.teste;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.example.teste.dtoout.TabcDTOout;
import com.example.teste.dtoout.TabdDTOout;
import com.example.teste.models.Tabc;
import com.example.teste.models.Tabd;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteApplication implements CommandLineRunner {
    @PersistenceContext
    private EntityManager manager;

    public static void main(String[] args) {
        SpringApplication.run(TesteApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        CriteriaBuilder cb = manager.getCriteriaBuilder();
        CriteriaQuery<TabcDTOout> criteriaQueryTabC = cb.createQuery(TabcDTOout.class);
        Root<Tabc> tabc = criteriaQueryTabC.from(Tabc.class);
        criteriaQueryTabC
                .select(cb.construct(TabcDTOout.class,
                        tabc.get("id")
                        ,tabc.get("descricao")
                        ));
        
        
        TypedQuery<TabcDTOout> queryC = manager.createQuery(criteriaQueryTabC);
        
        CriteriaQuery<TabdDTOout> criteriaQueryTabD = cb.createQuery(TabdDTOout.class);        
        List<TabcDTOout> tabclist =  queryC.getResultList();


        Root<Tabd> tabd = criteriaQueryTabD.from(Tabd.class);
        criteriaQueryTabD
                .select(cb.construct(TabdDTOout.class,
                        tabd.get("itemDescription")
                        ));
        TypedQuery<TabdDTOout> queryD = manager.createQuery(criteriaQueryTabD);

        for (int i = 0; i < tabclist.size()-1; i++) {
            criteriaQueryTabD.where( cb.equal(tabd.get("tabc").get("id"), tabclist.get(i).getId()));
            tabclist.get(i).setItems(queryD.getResultList() );
        }
    }

}
